Configuration Réseau
================

Lancer les VMs
----------------
Sur la machine Hôte
```
./up.sh
```

Attention suite à un bug dans la box initiale, il faut faire
----------------
Dans chaque VMs
```
sudoedit /etc/ssh/sshd_config
PasswordAuthentication yes 
```

Recharger les VMs
----------------
Sur la machine Hôte
```
./reload.sh
```

Exécuter les configs Ansible pour l'IP
----------------
Dans chaque VMs
```
cd /vagrant/
./a.sh
```

Exécuter les configs Ansible pour les Routes
----------------
Dans chaque VMs
```
cd /vagrant/
./a.sh
```

Configuration de l'interface VM1-6
================
Dans la VM VM1-6
```
cd /mnt/partage
./configure-tun_vm1-6.sh
```
