#include "func_utils.h"

#include "tunalloc.h"
#include "ext-in.h"
#include "ext-out.h"

extern SOCKET sockIN;
extern SOCKET sockOUT;
extern SOCKET csockOUT;
int fd;

void startServer(int portIn, int portOut) {
    int child = fork();
    int status;

    if(child == 0) {
		ext_out(portIn);
        while(1) {
			read_write(csockOUT, fd);
		}

		exit(0);
    }

    int child2 = fork();

    if(child2 == 0) {
		ext_out(portOut);
        while(1) {
			read_write(fd, csockOUT);
		}

		exit(0);
    }

    while(wait(&status) != -1);

}

void startClient(int portIn, int portOut, char * host) {
    int child = fork();
    int status;

    if(child == 0) {
		ext_in(host, portIn);
		while(1) {
			read_write(fd, sockIN);
		}

		exit(0);
    }

    int child2 = fork();

    if(child2 == 0) {
		ext_in(host, portOut);
		while(1) {
			read_write(sockIN, fd);
		}
		exit(0);
    }

    while(wait(&status) != -1);
}

int main (int argc, char** argv){

	if(argc < 3)
		return 1;

	fd = tun_alloc(argv[1]);


	if(atoi(argv[2]) == 1) {
		system("./configure-tun_vm3-6.sh");
		startServer(46815, 46816);
	} else if (atoi(argv[2]) == 2) {
		system("./configure-tun_vm1-6.sh");
		startClient(46815, 46816, "fc00:1234:2::36");
	} else
		return 1;

	close(sockIN);
	close(sockOUT);
	close(csockOUT);
	close(fd);
	return 0;
}