#include "func_utils.h"
#include "config.h"

char * iniRead(char *fichierINI, char *nom) {
	FILE * file = fopen(fichierINI, "rb"); 
	char *cmp = "=";

	if (file != NULL) { 
		char line [ 128 ];
 
		while ( fgets ( line, sizeof line, file ) != NULL ) {
			printf("Ligne : %s", line);

			char *key;
			key = strtok(line, cmp);

			if(key != NULL && strcmp(nom, key) == 0) {
				printf("Ligne : %s", key);
				key = strtok(NULL, cmp);
				printf("Ligne : %s", key);
				return key;
			}
		}
	} 

	fclose(file); 
	return NULL;
}