#include "func_utils.h"
#include "ext-in.h"

void ext_in(char *hostname, int port) {

	SOCKADDR_IN6 serv_addr;
    struct hostent *server;

	sockIN = socket(AF_INET6, SOCK_STREAM, 0);
	if(sockIN == INVALID_SOCKET) {
	    perror("socket()");
	    exit(errno);
	}

    server = gethostbyname2(hostname, AF_INET6);
    if (server == NULL) {
        fprintf(stderr, "ERROR, no such host\n");
        exit(0);
    }

	memset((char *) &serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin6_flowinfo = 0;
    serv_addr.sin6_family = AF_INET6;
    memmove((char *) &serv_addr.sin6_addr.s6_addr, (char *) server->h_addr, server->h_length);
    serv_addr.sin6_port = htons(port);

    //Sockets Layer Call: connect()
    if (connect(sockIN, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
	    perror("connect()");
	    exit(errno);
	}

}