#include "func_utils.h"
#include "ext-out.h"

void ext_out(int port) {
	sockOUT = socket(AF_INET6, SOCK_STREAM, 0);

	SOCKADDR_IN6 sin = { 0 };
	SOCKADDR_IN6 csin = { 0 };
	size_t sinsize = sizeof csin;

	sin.sin6_flowinfo = 0;
    sin.sin6_family = AF_INET6;
    sin.sin6_addr = in6addr_any;
    sin.sin6_port = htons(port);

	if(sockOUT == INVALID_SOCKET) {
	    perror("socket()");
	    exit(errno);
	}

	if(bind (sockOUT, (SOCKADDR *) &sin, sizeof sin) == SOCKET_ERROR) {
	    perror("bind()");
	    exit(errno);
	}

	if(listen(sockOUT, 5) == SOCKET_ERROR) {
	    perror("listen()");
	    exit(errno);
	}

	csockOUT = accept(sockOUT, (SOCKADDR *)&csin, (socklen_t*) &sinsize);

	if(csockOUT == INVALID_SOCKET) {
	    perror("accept()");
	    exit(errno);
	}
}