#include "func_utils.h"

void read_write (int src, int dst) {
    char buf[2048];

    int r = read (src, buf, 2048);

    while (r > 0) {
      int w = write (dst, buf, r);
      if (w < 0) {
        perror (__func__);     
      }

      r = read (src, buf, 2048);
    }
}