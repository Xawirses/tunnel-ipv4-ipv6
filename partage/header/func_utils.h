#ifndef __FUNC_UTILS_H__
#define __FUNC_UTILS_H__

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/socket.h> 
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <netdb.h>
#include <netinet/in.h>

#include <fcntl.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#include <errno.h>


typedef int SOCKET;
typedef struct sockaddr_in6 SOCKADDR_IN6;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;

#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define h_addr h_addr_list[0]

void read_write (int src, int dst);

#endif