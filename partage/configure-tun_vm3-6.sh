#! /bin/sh
ip addr add 172.16.2.1/28 dev tun0
ip route add 172.16.2.144/28 via 172.16.2.1
ip route add 172.16.2.128/28 via 172.16.2.1