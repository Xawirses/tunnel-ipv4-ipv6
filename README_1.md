Configuration Réseau
================

Lancer les VMs
----------------
Sur la machine Hôte
```
./up.sh
```

Attention suite à un bug dans la box initiale, il faut faire
----------------
Dans chaque VMs
```
sudoedit /etc/ssh/sshd_config
PasswordAuthentication yes 
```

Recharger les VMs
----------------
Sur la machine Hôte
```
./reload.sh
```

Exécuter les configs Ansible pour l'IP
----------------
Dans chaque VMs
```
cd /vagrant/
./a.sh
```

Exécuter les configs Ansible pour les Routes
----------------
Dans chaque VMs
```
cd /vagrant/
./a.sh
```

Test Réseau
================
Dans les VMs connectés sur un réseaux IPV4
```
cd /mnt/partage
./ping_IPV4.sh
```
Dans les VMs connectés sur un réseaux IPV6
```
cd /mnt/partage
./ping_IPV6.sh
```